#Region Header
;===================================================================================================
;
; Title:            TreePlant
; Filename:         TreePlant.au3
; Description:      ������� �� ������������ �������
; Version:          4.0.2.0
; Requirement(s):   Autoit 3.3.14.5
; Author(s):        Vint
; Date:             02.08.2023
;
;===================================================================================================
#EndRegion Header

#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
; #AutoIt3Wrapper_UseX64=y  ; ������������ X64 ������ AutoIt3_x64 ��� AUT2EXE_x64
; #AutoIt3Wrapper_Compile_both=y ; ������������� ��� �������� X86 � X64 �� ���
; #AutoIt3Wrapper_Run_SciTE_Minimized=n ; �������� SciTE ���� ������ �������. �� ��������� n
#AutoIt3Wrapper_Icon=centre.ico
#AutoIt3Wrapper_OutFile=TreePlant.exe
#AutoIt3Wrapper_OutFile_X64=TreePlantX64.exe
#AutoIt3Wrapper_OutFile_Type=exe
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y

#AutoIt3Wrapper_Res_Fileversion=4.0.2.0
#AutoIt3Wrapper_Res_LegalCopyright=(c)2015 Vint
#AutoIt3Wrapper_Res_Description=diagonal grid clicker
#AutoIt3Wrapper_Res_Comment=TreePlant
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_Res_requestedExecutionLevel=highestAvailable ; None, asInvoker (��� ������������), highestAvailable (���������� ���������� �������� ������������) ��� requireAdministrator (� ������� ��������������)
#AutoIt3Wrapper_Res_Field=Version|4.0.2.0
#AutoIt3Wrapper_Res_Field=Build|2023.08.02
#AutoIt3Wrapper_Res_Field=Coded by|Vint
#AutoIt3Wrapper_Res_Field=Compile date|%longdate% %time%
; #AutoIt3Wrapper_Res_Field=AutoIt Version|3.3.14.5
#AutoIt3Wrapper_Res_Field=AutoIt Version|%AutoItVers%
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#Region    **** AutoItSetOption ****
#RequireAdmin
Opt('MustDeclareVars', 1)
Opt('WinWaitDelay', 100)  ; ����� ����� �������� ������� �������. 250 ��
#EndRegion **** AutoItSetOption ****

#Region    **** Includes ****
#include <WindowsConstants.au3>
#include <GDIPlus.au3>
#include <GUIConstantsEx.au3>
#include <WinAPIEx.au3>
#include <Sound.au3>
#include <Misc.au3>
#EndRegion **** Includes ****

#Region    **** Global Constants and Variables ****
Global $TreePlantVersion = "version 4.0.2 of 02.08.2023"
Global $Dir, $StartDelay
Global $aPoints[5][2], $MouseOldX = -10, $MouseOldY = -10, $Poly = 0, $freeze = False
Global $baseX, $baseY, $SetX = -10, $SetY = -10, $dh, $dw, $field_1x, $field_1y, $field_2x, $field_2y, $nx, $ny, $ncx, $ncy, $GridSize
Global $step_big_x, $step_big_y, $step_small_x, $step_small_y
Global $hGUI, $hGraphic, $hPenRed, $hPenMagentaBold, $hBrushBlue, $hFormat, $hFamily, $hFont
Global $wait = 200, $music, $sSound, $aVual[4][2], $DelayAfterClick, $colortext, $hShem
Global $section = 'TreePlant'
Global $dl = 5              ; ������ "�������" ������� +-
Global $corX = 0, $corY = 0 ; ��������� ������ �� �������
Global $configFile = 'TreePlantConfig.ini'
#EndRegion **** Global Constants and Variables ****


_Singleton(@ScriptName)  ; ������ ������ ����� �����

If _WinAPI_GetAsyncKeyState(0x41) = True Then $MouseOldX = -10

If @Compiled Then
    FileInstall("Vint_avatar_11_64x64.jpg", @TempDir & "\\Vint_avatar_11_64x64.jpg")
    FileInstall("Legend.bmp", @TempDir & "\\Legend.bmp")
    FileInstall("cancel4.mp3", @TempDir & "\\cancel4.mp3")
    If $CmdLine[0] > 0 Then
        $section = $CmdLine[1]
    EndIf
    $Dir = @TempDir
Else
    $Dir = @ScriptDir
EndIf

HotKeySet('{ESC}', '_EndProgramm')
ReadingIni()

If $music Then
    FileInstall("sound3.mp3", @TempDir & "\\sound3.mp3")
    $sSound = _SoundOpen($Dir & "\\sound3.mp3")
    _SoundPlay($sSound, 0)
EndIf

Sleep($StartDelay)
Main()

Func Main()
    Vual()
    Shem()
    CreateWorkGUI()
    MainLoop()
    _EndProgramm()
EndFunc   ;==>Main


Func MainLoop()
    Do
        Local $pos = MouseGetPos() ; �������� ������� ���������� ��������� ����
        $baseX = $pos[0] - $field_1x + $corX
        $baseY = $pos[1] - $field_1y + $corY

        If $freeze Then
            If _WinAPI_GetAsyncKeyState(0x02) = True Then _EndProgramm() ; ������ ������ ������ ���� - �����

            If _WinAPI_GetAsyncKeyState(0x01) = False Then        ; �������� ����� ������ ����
                If ($SetX = $baseX) And ($SetY = $baseY) Then
                    ProClick()
                    Sleep(300)
                EndIf
                $freeze = False
            EndIf
        EndIf

        If Not ($MouseOldX = $baseX And $MouseOldY = $baseY) Then
            $MouseOldX = $baseX
            $MouseOldY = $baseY
            If $Poly = 1 Then
                _WinAPI_RedrawWindow($hGUI)
            Else
                $Poly = 1
            EndIf
            Draw()
        EndIf

        ; ��������� ������� �����
        If _WinAPI_GetAsyncKeyState(0x02) = True Then _EndProgramm() ; ������ ������ ������ ���� - �����

        If _WinAPI_GetAsyncKeyState(0x53) = True And Not $freeze Then IncX() ; ������ S
        If _WinAPI_GetAsyncKeyState(0x57) = True And Not $freeze Then DecX() ; ������ W
        If _WinAPI_GetAsyncKeyState(0x44) = True And Not $freeze Then IncY() ; ������ D
        If _WinAPI_GetAsyncKeyState(0x41) = True And Not $freeze Then DecY() ; ������ A
        If _WinAPI_GetAsyncKeyState(0x58) = True And Not $freeze Then ChangeGridSize() ; ������ X

        ; ��������� ������� ����
        If _WinAPI_GetAsyncKeyState(0x01) = True And Not $freeze Then        ; ������ ����� ������ ����
            $SetX = $baseX
            $SetY = $baseY
            $freeze = True
        EndIf
    Until Not Sleep(20)
EndFunc   ;==>MainLoop


Func CreateWorkGUI()
    $hGUI = GUICreate('', $field_2x-$field_1x, $field_2y-$field_1y, $field_1x, $field_1y, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_LAYERED, $WS_EX_TOPMOST)) ; BitOR($WS_EX_TRANSPARENT, $WS_EX_LAYERED, $WS_EX_TOPMOST, $WS_EX_TOOLWINDOW)
    GUISetBkColor(0xABABAB)
    GUISetState(@SW_SHOWNOACTIVATE)
    _WinAPI_SetLayeredWindowAttributes($hGUI, '0xABABAB', 0, $LWA_COLORKEY)
    _GDIPlus_Startup()
    $hGraphic = _GDIPlus_GraphicsCreateFromHWND($hGUI)
    $hPenRed = _GDIPlus_PenCreate(0xFFFF0000) ; ����� ��� ������� �����
    $hPenMagentaBold = _GDIPlus_PenCreate(0xFFFF00FF, 2) ; ����� ��� ������� �����
    $hBrushBlue = _GDIPlus_BrushCreateSolid($colortext) ; ����� �����
    $hFormat = _GDIPlus_StringFormatCreate() ; ������ ������
    $hFamily = _GDIPlus_FontFamilyCreate('Tahoma') ; ����� ������
    $hFont = _GDIPlus_FontCreate($hFamily, 14) ; ������ ������
    $aPoints[0][0] = 4
EndFunc   ;==>CreateWorkGUI


Func ProClick()
    Local $iX, $iY, $clX, $clY

    For $iY = 0 To $ncy
        For $iX = 0 To $ncx
            $clX = $baseX + ($iY-$iX)*$dw + $field_1x - $corX
            $clY = $baseY + ($iY+$iX)*$dh + $field_1y - $corY
            If $clX > $field_1x And $clX < $field_2x And $clY > $field_1y And $clY < $field_2y Then
                MouseClick("left", $clX, $clY, 1, $DelayAfterClick) ; ���� ���
            EndIf
        Next
    Next
EndFunc   ;==>ProClick


Func ReadingIni()
    Local $run_count

    $StartDelay = IniRead($configFile, $section, 'start_delay_ms', '5')
    $field_1x = IniRead($configFile, $section, 'field_1x', '100')
    $field_1y = IniRead($configFile, $section, 'field_1y', '100')
    $field_2x = @DesktopWidth + IniRead($configFile, $section, 'field_2x', '-100')
    $field_2y = @DesktopHeight + IniRead($configFile, $section, 'field_2y', '-100')
    $colortext = IniRead($configFile, $section, 'colortext', 0xFF0000FF)
    $music = IniRead($configFile, $section, 'music', 'True')
    $DelayAfterClick = IniRead($configFile, $section, 'delay_after_click_ms', '5')

    $step_big_x = IniRead($configFile, $section, 'step_big_x', '48')
    $step_big_y = IniRead($configFile, $section, 'step_big_y', '24')
    $step_small_x = IniRead($configFile, $section, 'step_small_x', '32')
    $step_small_y = IniRead($configFile, $section, 'step_small_y', '16')

    $GridSize = IniRead($configFile, $section, 'grid_size', 'small')
    $nx = IniRead($configFile, $section, 'nx', '3')
    $ny = IniRead($configFile, $section, 'ny', '4')
    $run_count = IniRead($configFile, $section, 'run_count', '0')
    $run_count += 1
    IniWrite($configFile, $section, 'run_count', $run_count)

    If $music And ($run_count/10 - Int($run_count/10) = 0) Then
        $music = True
    Else
        $music = False
    EndIf

    If $DelayAfterClick < 0 Then $DelayAfterClick = 0
    If $DelayAfterClick > 100 Then $DelayAfterClick = 100

    Nc()
    SetStepGridValue()
EndFunc   ;==>ReadingIni


Func SetStepGridValue()
    If $GridSize = 'big' Then
        $dw = $step_big_x
        $dh = $step_big_y
    Else
        $dw = $step_small_x
        $dh = $step_small_y
    EndIf
EndFunc   ;==>SetStepGridValue


Func ChangeGridSize()
    If $GridSize = 'small' Then
        $GridSize = 'big'
        SetStepGridValue()
    Else
        $GridSize = 'small'
        SetStepGridValue()
    EndIf
    $MouseOldX = -10
    $MouseOldY = -10
    IniWrite($configFile, $section, 'grid_size', $GridSize)
    Sleep($wait)
EndFunc   ;==>ChangeGridSize


Func IncX()
    If $nx < 18 Then
        $nx += 1
        Nc()
        IniWrite($configFile, $section, 'nx', $nx)
        Sleep($wait)
    EndIf
EndFunc   ;==>IncX


Func IncY()
    If $ny < 38 Then
        $ny += 1
        Nc()
        IniWrite($configFile, $section, 'ny', $ny)
        Sleep($wait)
    EndIf
EndFunc   ;==>IncY


Func DecX()
    If $nx > 1 Then
        $nx -= 1
        Nc()
        IniWrite($configFile, $section, 'nx', $nx)
        Sleep($wait)
    EndIf
EndFunc   ;==>DecX


Func DecY()
    If $ny  > 1 Then
        $ny -= 1
        Nc()
        IniWrite($configFile, $section, 'ny', $ny)
        Sleep($wait)
    EndIf
EndFunc   ;==>DecY


Func Nc()
    $ncx = $nx - 1
    $ncy = $ny - 1
    If $ncx < 0 Then
        $ncx = 0
        $nx = 1
    EndIf
    If $ncy < 0 Then
        $ncy = 0
        $ny = 1
    EndIf
    $MouseOldX = -10
    $MouseOldY = -10
EndFunc   ;==>Nc


Func Draw()
    Local $iX, $iY, $tLayout

    $aPoints[1][0] = $baseX
    $aPoints[1][1] = $baseY
    $aPoints[2][0] = $baseX - $ncx*$dw
    $aPoints[2][1] = $baseY + $ncx*$dh
    $aPoints[3][0] = $baseX + ($ncy-$ncx)*$dw
    $aPoints[3][1] = $baseY + ($ncx+$ncy)*$dh
    $aPoints[4][0] = $baseX + $ncy*$dw
    $aPoints[4][1] = $baseY + $ncy*$dh
    _GDIPlus_GraphicsDrawPolygon($hGraphic, $aPoints, $hPenRed)

    For $iY = 0 To $ncy
        For $iX = 0 To $ncx
            _GDIPlus_GraphicsDrawEllipse($hGraphic, ($baseX + ($iY-$iX)*$dw)-2, ($baseY + ($iY+$iX)*$dh)-2, 4, 4, $hPenMagentaBold) ; �����
        Next
    Next

    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX, $baseY-$dl-$dh*2, $baseX, $baseY+$dl-$dh*2, $hPenRed)
    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX-$dl, $baseY-$dh*2, $baseX+$dl, $baseY-$dh*2, $hPenRed)

    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX-$dw, $baseY-$dl-$dh, $baseX-$dw, $baseY+$dl-$dh, $hPenRed)
    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX-$dl-$dw, $baseY-$dh, $baseX+$dl-$dw, $baseY-$dh, $hPenRed)

    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX+$dw, $baseY-$dl-$dh, $baseX+$dw, $baseY+$dl-$dh, $hPenRed)
    _GDIPlus_GraphicsDrawLine($hGraphic, $baseX-$dl+$dw, $baseY-$dh, $baseX+$dl+$dw, $baseY-$dh, $hPenRed)

    ; ���������� ��� ������� � ������ �������
    $tLayout = _GDIPlus_RectFCreate($baseX + 25, $baseY - 12, 0, 0)
    ; ����� ������� � ���������
    _GDIPlus_GraphicsDrawStringEx($hGraphic, $nx & ' x ' & $ny & ' ' & $GridSize, $hFont, $tLayout, $hFormat, $hBrushBlue)
EndFunc   ;==>Draw


Func Vual()
    $aVual[0][0] = False
    $aVual[1][0] = False
    $aVual[2][0] = False
    $aVual[3][0] = False
    If $field_1y > 0 Then
        $aVual[0][0] = True
    EndIf
    If $field_2y < @DesktopHeight Then
        $aVual[1][0] = True
    EndIf
    If $field_1x > 0 Then
        $aVual[2][0] = True
    EndIf
    If $field_2x < @DesktopWidth Then
        $aVual[3][0] = True
    EndIf

    ; MsgBox(4096,'@DesktopWidth', '@DesktopWidth  '&@DesktopWidth &@CRLF& '@DesktopHeight  '&@DesktopHeight)
    ; BitOR($WS_EX_TRANSPARENT, $WS_EX_LAYERED, $WS_EX_TOPMOST)
    If $aVual[0][0] Then
        $aVual[0][1] = GUICreate("", @DesktopWidth, $field_1y, 0, 0, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_TOOLWINDOW, $WS_EX_TOPMOST))
        GUISetBkColor(0x000000, $aVual[0][1])
        WinSetTrans($aVual[0][1], '', 100)
        GUISetState(@SW_SHOWNOACTIVATE, $aVual[0][1])
    EndIf
    If $aVual[1][0] Then
        $aVual[1][1] = GUICreate("", @DesktopWidth, @DesktopHeight-$field_2y, 0, $field_2y, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_TOOLWINDOW, $WS_EX_TOPMOST))
        GUISetBkColor(0x000000, $aVual[1][1])
        WinSetTrans($aVual[1][1], '', 100)
        GUISetState(@SW_SHOWNOACTIVATE, $aVual[1][1])
    EndIf
    If $aVual[2][0] Then
        $aVual[2][1] = GUICreate("", $field_1x, $field_2y-$field_1y, 0, $field_1y, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_TOOLWINDOW, $WS_EX_TOPMOST))
        GUISetBkColor(0x000000, $aVual[2][1])
        WinSetTrans($aVual[2][1], '', 100)
        GUISetState(@SW_SHOWNOACTIVATE, $aVual[2][1])
    EndIf
    If $aVual[3][0] Then
        $aVual[3][1] = GUICreate("", @DesktopWidth-$field_2x, $field_2y-$field_1y, $field_2x, $field_1y, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_TOOLWINDOW, $WS_EX_TOPMOST))
        GUISetBkColor(0x000000, $aVual[3][1])
        WinSetTrans($aVual[3][1], '', 100)
        GUISetState(@SW_SHOWNOACTIVATE, $aVual[3][1])
    EndIf
    ; MsgBox(4096,"���� ����������", @DesktopWidth&'  '&$field_1y&'  '&0&'  '&0 &@CRLF& @DesktopWidth&'  '&@DesktopHeight-$field_2y&'  '&0&'  '&$field_2y &@CRLF& $field_1x&'  '&$field_2y-$field_1y&'  '&0&'  '&$field_1y &@CRLF& @DesktopWidth-$field_2x&'  '&$field_2y-$field_1y&'  '&$field_2x&'  '&$field_1y)
EndFunc   ;==>Vual


Func Shem()
    ;If @DesktopWidth-$field_2x < 130 Then Return
    $hShem = GUICreate("", 274, 130, @DesktopWidth-274, @DesktopHeight-130-40, BitOR($WS_DISABLED, $WS_POPUP), BitOR($WS_EX_TRANSPARENT, $WS_EX_TOOLWINDOW, $WS_EX_TOPMOST))
    WinSetTrans($hShem, '', 250)
    GUICtrlCreatePic($Dir & "\\Legend.bmp", 0, 0, 274, 130)
    GUISetState(@SW_SHOWNOACTIVATE, $hShem)
EndFunc   ;==>Shem


Func _EndProgramm()
    Local $FormEnd, $i, $hParent, $SoundCancel, $nMsg

    ; ���� � ������� ������
    If $music Then
        _SoundStop($sSound)
        _SoundClose($sSound)
        FileDelete(@TempDir & "\\sound3.mp3")
    EndIf
    GUIDelete($hShem)

    ; ������� ����������
    For $i = 0 To 3
        If $aVual[$i][0] Then GUIDelete($aVual[$i][1])
    Next

    ; �������� ��������
    FileDelete(@TempDir & "\\Legend.bmp")
    _GDIPlus_FontDispose($hFont)
    _GDIPlus_FontFamilyDispose($hFamily)
    _GDIPlus_StringFormatDispose($hFormat)
    _GDIPlus_BrushDispose($hBrushBlue)
    _GDIPlus_PenDispose($hPenMagentaBold)
    _GDIPlus_GraphicsDispose($hGraphic)
    _GDIPlus_Shutdown()
    GUIDelete($hGUI)

    $hParent  = GUICreate('')
    $FormEnd = GUICreate("� ���������", 200, 160, -1, -1, BitOR($WS_DISABLED, $WS_POPUP), $WS_EX_TOPMOST, $hParent)
    GUICtrlCreatePic($Dir & "\\Vint_avatar_11_64x64.jpg", 68, 8, 64, 64)
    GUICtrlCreateLabel("Autor Vint", 68, 80, 70, 20)
    GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
    GUICtrlSetColor(-1, 0x800080)
    GUICtrlCreateLabel($TreePlantVersion, 46, 95, 125, 17)
    GUICtrlSetFont(-1, 8, 400, 0, "Tahoma")
    GUICtrlCreateLabel("TreePlant", 76, 116, 82, 17)
    GUICtrlCreateLabel("����� �� �����", 57, 130, 100, 17)
    WinSetTrans($FormEnd, '', 0) ; ������������ 30
    GUISetBkColor(0xDCF2CA, $FormEnd)
    GUISetState(@SW_SHOW)

    For $i = 0 To 255 Step 8
        WinSetTrans($FormEnd, '', $i)
        Sleep(1)
    Next
    $i = 1024
    While $i >= 0
        If $i < 256 Then WinSetTrans($FormEnd, '', $i)

        $nMsg = GUIGetMsg()
        Switch $nMsg
            Case $GUI_EVENT_CLOSE
                Exit
        EndSwitch
        Sleep(1)
        $i -= 8
    WEnd

    ; �������� ���������� ��������
    $SoundCancel = _SoundOpen($Dir & "\\cancel4.mp3")
    _SoundPlay($SoundCancel, 1)
    _SoundClose($SoundCancel)
    FileDelete(@TempDir & "\\cancel4.mp3")
    FileDelete(@TempDir & "\\Vint_avatar_11_64x64.jpg")

    Exit
EndFunc ;==>_EndProgramm
