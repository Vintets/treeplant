
# Проект  TreePlant (AutoIt)

---------------------------------------------------------


Кликер по диагональной сетке.  
Проект начинался как "Посадка деревьев в Зомби Ферме" и был впоследствии расширен до универсального.

Особенности:
> Визуальный контроль.  
> Интерактивное наложение сетки.  
> Изменение параметров.


## Description

Есть 1 секунда по умолчанию для перехода в нужное окно (задаётся в настройках).  
Легенда справа внизу:
 - WASD - изменение размеров сетки,
 - X - переключение между двумя размерами сетки big (36x18) и small (24x12) (задаётся в настройках).

Кликать будет в пределах незатемнённой области (задаётся в настройках).  
Можно регулировать скорость кликов меняя задержку (задаётся в настройках).  
ESC или ПКМ - выход из программы


## Конфигурирование Configuration
`TreePlantConfig.ini`

```ini
[TreePlant]

start_delay_ms=1000
field_1x=100
field_1y=100
field_2x=-100
field_2y=-100
colortext=0xFF0000FF
music=True
delay_after_click_ms=3
step_big_x=36
step_big_y=18
step_small_x=24
step_small_y=12
```
``start_delay_ms`` : Задержка при старте в миллисекундах (чтобы было время переключиться на целевое окно)  
``field_1x/field_1y/field_2x/field_2y`` : Отступы слева/сверху/справа/снизу задающие рабочую область  
``colortext`` : Цвет надписи выбранного режима в формате 0xTTRRGGBB (прозрачность+RGB))  
``music`` : Проигрывать музыку каждый 10 запуск  
``delay_after_click_ms`` : Задержка после клика в миллисекундах (регулирование скорости прокликивания)  
``step_big_x/step_big_y`` : Шаг сетки в px по x/y на режиме 'big'  
``step_small_x/step_small_y`` : Шаг сетки в px по x/y на режиме 'small'


## Скриншоты

ver 4.0.2  
![Скриншот работы скрипта](https://github.com/Vintets/TreePlant/raw/master/screenshots/2023-08-01_16-03-16_v4.0.2_screenshot_1.png)

ver 3.0.0 в Зомби Ферма  
![Скриншот работы скрипта в Зомби Ферме](https://github.com/Vintets/TreePlant/raw/master/screenshots/2015-02-17_08-55-49_v3.0.0_screenshot_2.png)

## License

[![License](https://img.shields.io/badge/license-MIT-green)](https://github.com/toorusr/sitemap-generator/tree/master/LICENSE)  
:license:  [MIT](https://github.com/toorusr/sitemap-generator/tree/master/LICENSE)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


____

:copyright: 2023 by Vint
____